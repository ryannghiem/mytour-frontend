<div class="hotel-box">
    <div class="media">
        <div class="media-left">
            <a href="">
                <img class="media-object hotel-image" src="{{asset('/images/pic-hotel-1.png')}}" alt="...">
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading hotel-name ellipsis ellipsis-1 ellipsis-1lines">
                <a href="" class="text-blue" title="Khách Sạn Nam Hải Resort tại thành phố Hà Nội - Việt Nam">Khách Sạn Nam Hải Resort tại thành phố Hà Nội - Việt Nam</a>
            </h4>
            <div class="star-number">
                <span><img src="{{asset('/images/star-icon.png')}}"></span>
            </div>
            <div class="hotel-address ellipsis ellipsis-1 ellipsis-1lines">
                Thôn 1, Xã Điện Dương, Huyện Điện Bản, Tỉnh Quảng Nam, Việt Nam
            </div>
        </div>
        <div class="media-right text-right">
            <div class="hotel-discount text-bold">Giảm <span class="text-red">30%</span></div>
            <div class="hotel-price text-bold text-green">10.705.000đ</div>
        </div>
    </div>
</div>