<div class="hotel-box">
    <div class="media">
        <div class="media-left">
            <a href="">
                <img class="media-object hotel-image" src="{{asset($hotel->image_small ? $hotel->image_small : $hotel->image_origin)}}" alt="{!! $hotel->hotel_name !!}">
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading hotel-name ellipsis ellipsis-1 ellipsis-1lines">
                <a href="" class="text-blue" title="{!! $hotel->hotel_name !!}">{!! $hotel->hotel_name !!}</a>
            </h4>
            <div class="star-number">
                @for($star = 0; $star < $hotel->stars; $star ++)
                    <span><img src="{{asset('/images/star-icon.png')}}"></span>
                @endfor
            </div>
            <div class="hotel-address ellipsis ellipsis-1 ellipsis-1lines">{!! $hotel->address !!}</div>
            @if(!empty($hotel->last_booking))
            <div class="last-booking ellipsis ellipsis-1 ellipsis-1lines">
                Được đặt gần nhất lúc <span>{{date("H:i A", strtotime($hotel->last_booking))}} ngày {{date("d/m", strtotime($hotel->last_booking))}}</span>
            </div>
            @endif
        </div>
    </div>
</div>