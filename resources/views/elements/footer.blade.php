<div class="footer-hotline">
    <div class="hotline-info">
        <div class="hotline-brand">HOTLINE</div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="hotline-text">
                        <span class="text-red">HÀ NỘI</span> : (04) 3978 5397 - (04) 3974 7873 - (04) 3974 7885
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="hotline-text">
                        <span class="text-red">HỒ CHÍ MINH</span> : (08) 2229 0854 - 090 335 5749
                    </div>
                </div>
            </div>
        </div>
        <div class="customer-popup">
            <a href=""><img src="{{asset('/images/customer-popup.png')}}"></a>
        </div>
    </div>
</div>
<footer class="mytour-footer">
    <div class="container about-box">
        <div class="row">
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-4">
                        <ul class="mytour-about">
                            <li class="text-bold">VỀ MYTOUR</li>
                            <li><a href="">Giới thiệu</a></li>
                            <li><a>Điều khoản sử dụng</a></li>
                            <li><a href="">Hỏi đáp</a></li>
                            <li><a href="">Tin Tức</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <ul class="mytour-about">
                            <li class="text-bold">DÀNH CHO BẠN</li>
                            <li><a href="">Điểm thưởng</a></li>
                            <li><a>Khuyến mãi</a></li>
                            <li><a href="">Trợ giúp</a></li>
                            <li><a href="">Liên hệ</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <ul class="mytour-social">
                            <li class="text-bold">CHIA SẺ</li>
                            <li><a href="">
                                    <span class="btn btn-social-icon btn-facebook">
                                    <i class="fa fa-facebook"></i>
                                    </span> <span>Facebook</span>
                                </a>
                            </li>
                            <li><a href="">
                                    <span class="btn btn-social-icon btn-google">
                                        <i class="fa fa-google-plus"></i>
                                    </span> <span>Google +</span>
                                </a>
                            </li>
                            <li><a href="">
                                    <span class="btn btn-social-icon btn-twitter">
                                        <i class="fa fa-twitter"></i>
                                    </span> <span>Twitter</span>
                                </a>
                            </li>
                            <li><a href="">
                                    <span class="btn btn-social-icon btn-youtube">
                                        <i class="fa fa-youtube"></i>
                                    </span> <span>Youtube</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mytour-subscribe">
                <div class="form-group text-center">Đăng kí email để nhận khuyến mại lên đến <span class="text-green text-bold">70%</span></div>
                <form class="form-group form-inline form-subscribe text-right" method="post" action="">
                    <div class="form-group">
                        <input type="email" class="form-control" id="subscribeEmail" name="email" placeholder="example@email.com">
                    </div>
                    <button type="submit" class="btn btn-success">Đăng ký</button>
                </form>
                <div class="text-right">
                    <img src="{{asset('/images/partner-verified.png')}}">
                </div>
            </div>
        </div>
    </div>
    @if(!empty($data['cities']))
    <div class="container cities-list">
        <div class="text-blue text-bold form-group">KHÁCH SẠN THEO THÀNH PHỐ </div>
        <div>
            @foreach($data['cities'] as $city)
                <a class="city-item {{$city->is_hot ? 'city-hot' : ''}}">{!! $city->name !!}</a>
            @endforeach
        </div>
    </div>
    @endif
    <div class="container footer-address">
        <div class="row">
            <div class="col-sm-2">
                <img src="{{asset('/images/logo-footer.png')}}" alt="">
            </div>
            <div class="col-sm-5">
                <div class="text-bold margin-top-5">Công ty TNHH Mytour Việt Nam</div>
                <div class="footer-address-content">Số GCNDT: 0105983269 cấp ngày 30/08/2012</div>
                <div class="footer-address-content">Trụ sở chính: 51 Lê Đại Hành, Hai Bà Trưng, Hà Nội</div>
                <div class="footer-address-content">Chi nhánh HCM: Tầng 4, Lữ Gia Plaza, 70 Lữ Gia, P.13, Q.11, Hồ Chí Minh</div>
            </div>
            <div class="col-sm-5">
                <div class="text-bold margin-top-5">Dịch vụ</div>
                <div class="footer-address-content">Khách sạn Việt Nam, Dịch vụ đặt phòng, đặt vé tàu, vé máy bay</div>
                <div class="footer-address-content">Tour du lịch, thông tin địa danh, địa điểm du lịch</div>
                <div class="footer-address-content">Thông tin nhà hàng, địa điểm giải trí</div>
            </div>
        </div>
    </div>
</footer>