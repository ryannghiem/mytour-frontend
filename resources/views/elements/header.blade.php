<header class="bs-docs-nav navbar">
    <div class="header-top hidden-xs">
        <div class="container">
            <div class="header-top-left">
                <ul>
                    <li><a href="" >Vatgia.com</a></li>
                    <li><a href="" >Thanh toán trực tuyến</a></li>
                    <li><a href="" >Hàng giá rẻ</a></li>
                    <li><a href="" >Hàng chĩnh hãng giá tốt</a></li>
                    <li><a href="" >Thêm <i class="fa fa-angle-double-right"></i></a></li>
                </ul>
            </div>
            <div class="header-top-right">
                <ul>
                    <li><a href="" >Tin tức</a></li>
                    <li><a href="" >Hỏi đáp</a></li>
                    <li><a href="" >Đăng nhập</a></li>
                    <li><a href="" >Đăng kí</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container visible-xs-block">
        <div class="navbar-header">
            <button aria-controls="bs-navbar" aria-expanded="true" class="navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand header-brand"><img src="{{asset('/images/logo.png')}}" alt="Mytour Việt Nam"></a>
        </div>
        <nav class="navbar-collapse collapse" id="bs-navbar" aria-expanded="true">
            <ul class="nav navbar-nav">
                <li><a href="" >Vatgia.com</a></li>
                <li><a href="" >Thanh toán trực tuyến</a></li>
                <li><a href="" >Hàng giá rẻ</a></li>
                <li><a href="" >Hàng chĩnh hãng giá tốt</a></li>
                <li><a href="" >Thêm <i class="fa fa-angle-double-right"></i></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="" >Tin tức</a></li>
                <li><a href="" >Hỏi đáp</a></li>
                <li><a href="" >Đăng nhập</a></li>
                <li><a href="" >Đăng kí</a></li>
            </ul>
        </nav>
    </div>
    <div class="header-mid hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <a href="/"><img src="{{asset('/images/logo.png')}}" alt="Mytour Việt Nam"></a>
                </div>
                <div class="col-sm-7 text-right ">
                    <div class="header-hotline-icon hidden-sm">
                        <img src="{{asset('/images/phone.png')}}" alt="Mytour Việt Nam"> HOTLINE
                    </div>
                    <div class="header-hotline-address text-left">
                        <div>HÀ NỘI</div>
                        <div>HỒ CHÍ MINH</div>
                    </div>
                    <div class="header-hotline-number  text-left">
                        <div>(04) 3978 5397 - (04) 3974 7885 - 098 674 6861</div>
                        <div>(08) 2229 0854 - 090 335 5749</div>
                    </div>
                </div>
                <div class="col-sm-3 text-right header-support-online">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-default hidden-sm hidden-xs">Hỗ trợ trực tuyến</button>
                        <button type="button" class="btn btn-default"><a href=""><img src="{{asset('/images/yahoo-icon.png')}}"></a></button>
                        <button type="button" class="btn btn-default"><a href=""><img src="{{asset('/images/skype-icon.png')}}"></a></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-menu">
        <div class="container">
            <ul class="nav nav-pills">
                <li role="presentation" class="nav-home active"><a href="#" class="button-home"><img src="{{asset('/images/home-icon.png')}}"></a><span class="caret"></span></li>
                <li role="presentation"><a href="#">Khách sạn</a></li>
                <li role="presentation"><a href="#">Tour</a></li>
                <li role="presentation"><a href="#">Khuyến mại</a></li>
                <li role="presentation"><a href="#" class="header-menu-best">BEST SAVING</a></li>
            </ul>
            <div class="select-lang">
                <select class="selectpicker">
                    <option data-content="<img src='{{asset('/images/vietnam-icon.png')}}'> Việt Nam"></option>
                    <option data-content="<img src='{{asset('/images/english-icon.png')}}'> English"></option>
                </select>
                <select class="select-cur">
                    <option>Vnđ</option>
                    <option>$</option>
                </select>
            </div>
        </div>
    </div>
</header>