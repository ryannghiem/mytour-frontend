<div class="search-box">
    <div class="search-header box-header">
        <span>BẠN MUỐN ĐẾN ĐÂU ?</span>
    </div>
    <div class="search-body">
        <form action="" method="post">
            <div class="form-group">
                <label class="radio-inline text-bold">
                    <input type="radio" name="searchType" value="1" class="search-type" checked="checked"> Khách sạn
                </label>
                <label class="radio-inline text-bold">
                    <input type="radio" name="searchType" value="2" class="search-type"> Tour
                </label>
            </div>
            <div class="form-group form-hotel">
                <input type="text" class="form-control" name="keyword" placeholder="Khách sạn, Thành phố...">
            </div>
            <div class="form-group form-hotel">
                <div class="row">
                    <div class="col-sm-5 form-date has-feedback">
                        <label for="exampleInputEmail1">Ngày nhận phòng</label>
                        <input type="text" class="form-control datepicker-book" id="startBook" name="startBook" placeholder="Xin chọn ngày">
                        <span class="form-control-feedback" aria-hidden="true"></span>
                    </div>
                    <div class="col-sm-2 row">
                        <label for="exampleInputEmail1">Đêm</label>
                        <select class="form-control number-days" name="numberDays" id="numberDays">
                            <option>1</option>
                            <option>2</option>
                            <option selected="selected">3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="col-sm-5 form-date has-feedback">
                        <label for="exampleInputEmail1">Ngày trả phòng</label>
                        <input type="text" class="form-control datepicker-book" id="endBook" name="endBook" placeholder="Xin chọn ngày">
                        <span class="form-control-feedback" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
            <div class="form-group ">
                <div class="row">
                    <div class="col-sm-6 mytour-verify box-header">
                        <img src="{{asset('/images/verify-icon.png')}}"> <span>Mytour.vn đảm bảo giá tốt</span>
                    </div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary btn-block">TÌM KIẾM</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>