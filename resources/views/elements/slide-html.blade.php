<div id="carousel-example-generic" class="carousel slide home-slider" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <a href="">
                <img src="{{asset('/images/slider-2.png')}}" alt="">
                <div class="hotel-info">
                    <div class="hotel-discount hidden-sm">
                        Giảm 60%
                    </div>
                    <div class="hotel-stars">
                        <span><img src="{{asset('/images/star-icon.png')}}"></span>
                        <span><img src="{{asset('/images/star-icon.png')}}"></span>
                        <span><img src="{{asset('/images/star-icon.png')}}"></span>
                    </div>
                    <div class="hotel-name">Khách sạn Princess D'Annam Resort & Spa Bình Thuận</div>
                    <div class="slider-des">Giá: <span>3.058.000</span>đ/2N1Đ/2 người</div>
                </div>
            </a>
        </div>
        <div class="item">
            <a href="">
                <img src="{{asset('/images/slider-2.png')}}" alt="...">
            </a>
        </div>
        <div class="item">
            <a href="">
                <img src="{{asset('/images/slider-2.png')}}" alt="...">
            </a>
        </div>
    </div>

    @include('elements.search-box-html')
</div>
