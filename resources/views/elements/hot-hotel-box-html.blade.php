<div class="hotel-box">
    <div class="media">
        <div class="media-left">
            <a href="">
                <img class="media-object hotel-image" src="{{asset('/images/pic-hotel-1.png')}}" alt="...">
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading hotel-name ellipsis ellipsis-1 ellipsis-1lines">
                <a href="" class="text-blue" title="Khách Sạn Nam Hải Resort">Khách Sạn Nam Hải Resort</a>
            </h4>
            <div class="star-number">
                <span><img src="{{asset('/images/star-icon.png')}}"></span>
            </div>
            <div class="hotel-address ellipsis ellipsis-1 ellipsis-1lines">
                Thôn 1, Xã Điện Dương, Điện Bản, Quảng Nam
            </div>
            <div class="last-booking ellipsis ellipsis-1 ellipsis-1lines">
                Được đặt gần nhất lúc <span>09:16 AM ngày 04/05</span>
            </div>
        </div>
    </div>
</div>