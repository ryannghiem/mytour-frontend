<div class="hotel-box">
    <div class="media">
        <div class="media-left">
            <a href="">
                <img class="media-object hotel-image" src="{{asset($hotel->image_small ? $hotel->image_small : $hotel->image_origin)}}" alt="{!! $hotel->hotel_name !!}">
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading hotel-name ellipsis ellipsis-1 ellipsis-1lines">
                <a href="" class="text-blue" title="{!! $hotel->hotel_name !!}">{!! $hotel->hotel_name !!}</a>
            </h4>
            <div class="star-number">
                @for($star = 0; $star < $hotel->stars; $star ++)
                <span><img src="{{asset('/images/star-icon.png')}}"></span>
                @endfor
            </div>
            <div class="hotel-address ellipsis ellipsis-1 ellipsis-1lines">{!! $hotel->address !!}</div>
        </div>
        <div class="media-right text-right">
            <div class="hotel-discount text-bold">Giảm <span class="text-red">{{number_format($hotel->discount_percent)}}%</span></div>
            <div class="hotel-price text-bold text-green">{{number_format($hotel->discount_price)}}đ</div>
        </div>
    </div>
</div>