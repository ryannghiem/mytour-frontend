<div id="carousel-example-generic" class="carousel slide home-slider" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        @foreach ($data['slides'] as $index => $slide)
            <li data-target="#carousel-example-generic" data-slide-to="{{$index}}" class="{{$index == 0 ? 'active' : ''}}"></li>
        @endforeach
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        @foreach ($data['slides'] as $index => $slide)
            <div class="item {{$index == 0 ? 'active' : ''}}">
                <a href="">
                    <img src="{{asset($slide->image)}}" alt="{!! $slide->hotel_name !!}">
                    <div class="hotel-info">
                        @if(!empty($slide->room_has_discount))
                            <div class="hotel-discount hidden-sm">
                                Giảm {{number_format($slide->room_discount_percent)}}%
                            </div>
                        @endif
                        <div class="hotel-stars">
                            @for($i = 1; $i <= $slide->hotel_stars; $i++)
                                <span><img src="{{asset('/images/star-icon.png')}}"></span>
                            @endfor
                        </div>
                        <div class="hotel-name">{!! $slide->hotel_name !!}</div>
                        <div class="slider-des">Giá: <span>{{number_format(!empty($slide->room_has_discount) ? $slide->room_discount_price : $slide->room_price)}}</span>đ/2N1Đ/{{$slide->room_size}} người</div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>

    @include('elements.search-box-html')
</div>