<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
    <link href="{{asset('/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/css/bootstrap-select.css')}}" rel="stylesheet" >
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css')}}">
    <link href="{{asset('/vendor/bootstrap-social/bootstrap-social.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/css/main.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
@yield('before-header')
@include('elements.header')
<!--Page Body Start-->
@yield('before-body')
<div class="page-body">
    @yield('content')
</div>
<!-- /End Page Body -->
<!-- Footer Start -->
@yield('before-footer')
@include('elements.footer')
@yield('after-footer')
<script src="{{asset('/js/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('/vendor/moment/min/moment.min.js')}}"></script>
<script src="{{asset('/vendor/moment/locale/vi.js')}}"></script>
<script src="{{asset('/js/bootstrap.min.js')}}" type="text/javascript" async="" defer=""></script>
<script src="{{asset('/js/bootstrap-select.js')}}"></script>
<script src="{{asset('/vendor/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('/js/main.js')}}"></script>
</body>
</html>
