@extends('app')
@section('content')
    @if(!empty($data['slides']))
    <div class="homepage-slider hidden-xs">
        <div class="container">
            @include('elements.slides')
        </div>
    </div>
    @endif
    <div class="visible-xs col-sm-12 margin-top-5">
        @include('elements.search-box-html')
    </div>
    <div class="container margin-top-14">
        <div class="row">
            <div class="col-sm-6 padding-right-4 margin-bottom-10">
                <div class="home-box-content panel panel-default">
                    <div class="home-box-header box-header why-choice-mytour panel-heading">
                        <span>TẠI SAO CHỌN MYTOUR </span>
                    </div>
                    <div class="home-box-body">
                        <ul class="list-reasons">
                            <li><span class="mytour-icon icon-hotel margin-right-20"></span> Hơn <span
                                        class="text-green text-bold">5000</span> khách sạn trên khắp Việt Nam!
                            </li>
                            <li><span class="mytour-icon icon-discount margin-right-20"></span> Nhận <span
                                        class="text-green text-bold">2 - 5%</span> điểm thưởng mỗi lần đặt phòng.
                            </li>
                            <li><span class="mytour-icon icon-money margin-right-20"></span> <span
                                        class="text-green text-bold">Giá rẻ</span> hơn khi đặt phòng trực tiếp tại
                                khách sạn!
                            </li>
                            <li><span class="mytour-icon icon-gift margin-right-20"></span> Khuyến mãi lên tới <span
                                        class="text-green text-bold">70%</span></li>
                            <li><span class="mytour-icon icon-credit-card margin-right-20"></span> Thanh toán <span
                                        class="text-green text-bold">dễ dàng, đa dạng</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 padding-left-4 margin-bottom-10">
                <div class="home-box-content box-locate">
                    <div class="locate-picture">
                        <img src="{{asset('/images/picture-locate.png')}}">
                        @for ($i = 0; $i < 9; $i++)
                            <a href="" class="{{$i == 0 ? '' : 'hidden'}}" city-id="{{$data['cities_hot'][$i]->id}}">
                                <div class="city-selected">
                                    <div class="text-blue text-bold">{!! $data['cities_hot'][$i]->name !!}</div>
                                    <div class="hotel-count ellipsis ellipsis-1 ellipsis-1lines">Xem <span class="text-bold">{{$data['cities_hot'][$i]->hotel_count}}</span> khách sạn tại {!! $data['cities_hot'][$i]->name !!}</div>
                                </div>
                            </a>
                        @endfor
                    </div>
                    <ul class="list-group">
                        @for ($i = 0; $i < 3; $i++)
                            <li class="list-group-item clearfix">
                                @for ($j = 0; $j < 3; $j++)
                                <div class="col-xs-4">
                                    <a role="button" class="hotel-by-locate {{$i*3 + $j == 0 ? 'active' : ''}}" city-id="{{$data['cities_hot'][$i*3 + $j]->id}}"><span class="mytour-icon icon-arrow"></span> {!! $data['cities_hot'][$i*3 + $j]->name !!}</a>
                                </div>
                                @endfor
                            </li>
                        @endfor
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 padding-right-4 margin-bottom-10">
                <div class="home-box-content mytour-bonus panel panel-default">
                    <div class="home-box-header box-header panel-heading">
                        ĐIỂM THƯỞNG MYTOUR
                    </div>
                    <div class="home-box-body">
                        <div class="row">
                            <div class="col-sm-6 col-xs-7">
                                <ul class="list-bonus">
                                    <li>Giảm <span class="text-green text-bold">2 - 5%</span> cho mỗi đơn phòng</li>
                                    <li>Cộng tiền thưởng <span class="text-green text-bold">tự động</span></li>
                                    <li>Cơ hội đặt phòng <span class="text-green text-bold">miễn phí</span></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-xs-5 hidden-xxs">
                                <img src="{{asset('/images/bonus-gift.png')}}" class="bonus-gift">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 padding-left-4 margin-bottom-10">
                <div class="home-box-content mytour-app">
                    <div class="row">
                        <div class="col-sm-8 pull-right">
                            <div class="text-bold margin-bottom-10">Ứng dụng <span
                                        class="text-blue">Mytour.vn</span> trên điện thoại
                            </div>
                            <a class="btn btn-info download-app">Tải về</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(!empty($data['ads']))
        <div class="row">
            <div class="col-sm-12">
                <div class="as-box margin-bottom-10 clearfix">
                    @foreach($data['ads'] as $ads)
                    <div class="col-sm-4 margin-bottom-10">
                        <a href="{{$ads->ads_url}}">
                            <img src="{{asset(!empty($ads->image_small) ? $ads->image_small : $ads->image_origin)}}" alt="{!! $ads->name !!}">
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-sm-6 padding-right-4 margin-bottom-10">
                <div class="home-box-content new-promotion panel panel-default">
                    <div class="home-box-header box-header text-bold panel-heading">
                        KHUYẾN MÃI MỚI NHẤT
                    </div>
                    <div class="home-box-body">
                        <div class="promotion-locate">
                            <ul class="nav nav-pills">
                                @foreach($data['new_cities_top'] as $index => $city)
                                    <li role="presentation" class="{{$index == 1 ? 'active' : ''}}">
                                        <a href="#city{{$index}}" aria-controls="city{{$index}}" role="tab" data-toggle="tab" class="text-gray">{!! $city['name'] !!}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="promotion-hotels tab-content">
                            @foreach($data['new_cities_top'] as $index => $city)
                                <div role="tabpanel" class="tab-pane {{$index == 1 ? 'active' : ''}}" id="city{{$index}}">
                                @foreach($data['new_discount_hotels'] as $hotel)
                                    @if($hotel->city_id == $city['id'])
                                    @include('elements.hotel-box')
                                    @endif
                                @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 padding-left-4 margin-bottom-10">
                <div class="home-box-content panel panel-default">
                    <div class="home-box-header box-header text-bold panel-heading">
                        KHÁCH SẠN NỔI BẬT NHẤT TẠI HÀ NỘI
                    </div>
                    <div class="home-box-body">
                        <div class="hot-hotels-locate">
                            <ul class="nav nav-pills">
                                @foreach($data['hot_cities_top'] as $index => $city)
                                    <li role="presentation" class="{{$index == 1 ? 'active' : ''}}">
                                        <a href="#cityHot{{$index}}" aria-controls="cityHot{{$index}}" role="tab" data-toggle="tab" class="text-gray">{!! $city['name'] !!}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="hot-hotels tab-content">
                            @foreach($data['hot_cities_top'] as $index => $city)
                                <div role="tabpanel" class="tab-pane {{$index == 1 ? 'active' : ''}}" id="cityHot{{$index}}">
                                    @foreach($data['hot_hotels'] as $hotel)
                                        @if($hotel->city_id == $city['id'])
                                            @include('elements.hot-hotel-box')
                                        @endif
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection