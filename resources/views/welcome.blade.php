@extends('app')
@section('content')
    <div class="homepage-slider hidden-xs">
        <div class="container">
            @include('elements.slide-html')
        </div>
    </div>
    <div class="visible-xs col-sm-12 margin-top-5">
        @include('elements.search-box-html')
    </div>
    <div class="container margin-top-14">
        <div class="row">
            <div class="col-sm-6 padding-right-4 margin-bottom-10">
                <div class="home-box-content panel panel-default">
                    <div class="home-box-header box-header why-choice-mytour panel-heading">
                        <span>TẠI SAO CHỌN MYTOUR </span>
                    </div>
                    <div class="home-box-body">
                        <ul class="list-reasons">
                            <li><span class="mytour-icon icon-hotel margin-right-20"></span> Hơn <span
                                        class="text-green text-bold">5000</span> khách sạn trên khắp Việt Nam!
                            </li>
                            <li><span class="mytour-icon icon-discount margin-right-20"></span> Nhận <span
                                        class="text-green text-bold">2 - 5%</span> điểm thưởng mỗi lần đặt phòng.
                            </li>
                            <li><span class="mytour-icon icon-money margin-right-20"></span> <span
                                        class="text-green text-bold">Giá rẻ</span> hơn khi đặt phòng trực tiếp tại
                                khách sạn!
                            </li>
                            <li><span class="mytour-icon icon-gift margin-right-20"></span> Khuyến mãi lên tới <span
                                        class="text-green text-bold">70%</span></li>
                            <li><span class="mytour-icon icon-credit-card margin-right-20"></span> Thanh toán <span
                                        class="text-green text-bold">dễ dàng, đa dạng</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 padding-left-4 margin-bottom-10">
                <div class="home-box-content box-locate">
                    <div class="locate-picture">
                        <img src="{{asset('/images/picture-locate.png')}}">
                        <a href="">
                            <div class="city-selected">
                                <div class="text-blue text-bold">HÀ NỘI</div>
                                <div class="hotel-count">Xem <span class="text-bold">663</span> khách sạn tại Hà Nội</div>
                            </div>
                        </a>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item clearfix">
                            <div class="col-xs-4"><a href=""><span class="mytour-icon icon-arrow"></span> Hà Nội</a>
                            </div>
                            <div class="col-xs-4"><a href=""><span class="mytour-icon icon-arrow"></span> Hồ Chí
                                    Minh</a></div>
                            <div class="col-xs-4"><a href=""><span class="mytour-icon icon-arrow"></span> Nha Trang</a>
                            </div>
                        </li>
                        <li class="list-group-item clearfix">
                            <div class="col-xs-4"><a href=""><span class="mytour-icon icon-arrow"></span> Đà
                                    Nẵng</a></div>
                            <div class="col-xs-4"><a href=""><span class="mytour-icon icon-arrow"></span> Hội An</a>
                            </div>
                            <div class="col-xs-4"><a href=""><span class="mytour-icon icon-arrow"></span> Phan Thiết</a>
                            </div>
                        </li>
                        <li class="list-group-item clearfix">
                            <div class="col-xs-4"><a href=""><span class="mytour-icon icon-arrow"></span> Vũng
                                    Tàu</a></div>
                            <div class="col-xs-4"><a href=""><span class="mytour-icon icon-arrow"></span> Đà Lạt</a>
                            </div>
                            <div class="col-xs-4"><a href=""><span class="mytour-icon icon-arrow"></span> Hạ
                                    Long</a></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 padding-right-4 margin-bottom-10">
                <div class="home-box-content mytour-bonus panel panel-default">
                    <div class="home-box-header box-header panel-heading">
                        ĐIỂM THƯỞNG MYTOUR
                    </div>
                    <div class="home-box-body">
                        <div class="row">
                            <div class="col-sm-6 col-xs-7">
                                <ul class="list-bonus">
                                    <li>Giảm <span class="text-green text-bold">2 - 5%</span> cho mỗi đơn phòng</li>
                                    <li>Cộng tiền thưởng <span class="text-green text-bold">tự động</span></li>
                                    <li>Cơ hội đặt phòng <span class="text-green text-bold">miễn phí</span></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-xs-5 hidden-xxs">
                                <img src="{{asset('/images/bonus-gift.png')}}" class="bonus-gift">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 padding-left-4 margin-bottom-10">
                <div class="home-box-content mytour-app">
                    <div class="row">
                        <div class="col-sm-8 pull-right">
                            <div class="text-bold margin-bottom-10">Ứng dụng <span
                                        class="text-blue">Mytour.vn</span> trên điện thoại
                            </div>
                            <a class="btn btn-info download-app">Tải về</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="as-box margin-bottom-10 clearfix">
                    <div class="col-sm-4 margin-bottom-10">
                        <img src="{{asset('/images/pic-1.png')}}">
                    </div>
                    <div class="col-sm-4 margin-bottom-10">
                        <img src="{{asset('/images/pic-2.png')}}">
                    </div>
                    <div class="col-sm-4 margin-bottom-10">
                        <img src="{{asset('/images/pic-3.png')}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 padding-right-4 margin-bottom-10">
                <div class="home-box-content new-promotion panel panel-default">
                    <div class="home-box-header box-header text-bold panel-heading">
                        KHUYẾN MÃI MỚI NHẤT
                    </div>
                    <div class="home-box-body">
                        <div class="promotion-locate">
                            <ul class="nav nav-pills">
                                <li role="presentation" class="active"><a href="#">Hà Nội</a></li>
                                <li role="presentation"><a href="#" class="text-gray">Hồ Chí Minh</a></li>
                                <li role="presentation"><a href="#" class="text-gray">Nha Trang</a></li>
                                <li role="presentation"><a href="#" class="text-gray">Đà Lạt</a></li>
                                <li role="presentation"><a href="#" class="text-gray">Đà Nẵng</a></li>
                                <li role="presentation"><a href="#" class="text-gray">Phan Thiết</a></li>
                                <li role="presentation"><a href="#" class="text-gray">Huế</a></li>
                            </ul>
                        </div>
                        <div class="promotion-hotels">
                            @for ($i = 0; $i < 6; $i++)
                                @include('elements.hotel-box-html')
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 padding-left-4 margin-bottom-10">
                <div class="home-box-content panel panel-default">
                    <div class="home-box-header box-header text-bold panel-heading">
                        KHÁCH SẠN NỔI BẬT NHẤT TẠI HÀ NỘI
                    </div>
                    <div class="home-box-body">
                        <div class="hot-hotels-locate">
                            <ul class="nav nav-pills">
                                <li role="presentation" class="active"><a href="#">Hà Nội</a></li>
                                <li role="presentation"><a href="#" class="text-gray">Hồ Chí Minh</a></li>
                                <li role="presentation"><a href="#" class="text-gray">Nha Trang</a></li>
                                <li role="presentation"><a href="#" class="text-gray">Đà Lạt</a></li>
                                <li role="presentation"><a href="#" class="text-gray">Đà Nẵng</a></li>
                                <li role="presentation"><a href="#" class="text-gray">Phan Thiết</a></li>
                                <li role="presentation"><a href="#" class="text-gray">Huế</a></li>
                            </ul>
                        </div>
                        <div class="hot-hotels">
                            @for ($i = 0; $i < 6; $i++)
                                @include('elements.hot-hotel-box-html')
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection