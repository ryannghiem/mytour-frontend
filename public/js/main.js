$(document).ready(function(e){
    $('select.selectpicker').selectpicker();
    $('.datepicker-book').datetimepicker({
        format: "dddd, DD/MM/YYYY",
        locale: 'vi',
        showClear : true,
        useCurrent: true
    }).on("dp.change", function (e) {
        calculateBookingByPickDate();
    });
    $("#startBook").on("dp.change", function (e) {
        if (e.date){
            var enddate = moment(e.date, "dddd, DD/MM/YYYY").add(1, 'days');
            $('#endBook').data("DateTimePicker").minDate(enddate);
        }
        else $('#endBook').data("DateTimePicker").minDate(false);
    });
    $("#endBook").on("dp.change", function (e) {
        if (e.date) {
            $('#startBook').data("DateTimePicker").maxDate(e.date);
        }else $('#startBook').data("DateTimePicker").maxDate(false);
    });
    function calculateBookingByPickDate() {
        var $endDate = moment($('#endBook').val(), "dddd, DD/MM/YYYY");
        var $startDate = moment($('#startBook').val(), "dddd, DD/MM/YYYY");
        var $diffDate = $endDate.diff($startDate, 'days');
        $("#numberDays").val($diffDate);
    }
    $("#numberDays").on('change',function () {
        calculateBookingByPickNumber();
    });
    function calculateBookingByPickNumber() {
        var numberOfDays = $("#numberDays").val();
        if ($('#startBook').val()) {
            var $startDate = new Date(moment($('#startBook').val(), "dddd, DD/MM/YYYY").format('YYYY/MM/DD'));
            $startDate.setDate($startDate.getDate() + parseInt(numberOfDays));
            $('#endBook').val(moment($startDate).format('dddd, DD/MM/YYYY'));
        }else if ($('#endBook').val()){
            var $endDate = new Date(moment($('#endBook').val(), "dddd, DD/MM/YYYY").format('YYYY/MM/DD'));
            $endDate.setDate($endDate.getDate() - parseInt(numberOfDays));
            $('#startBook').val(moment($endDate).format('dddd, DD/MM/YYYY'));
        }
    }
    $('.ellipsis.ellipsis-1').on('click',function () {
        $(this).toggleClass('ellipsis-1lines')
    });
    $('.ellipsis.ellipsis-2').on('click',function () {
        $(this).toggleClass('ellipsis-2lines')
    });
    $('.ellipsis.ellipsis-3').on('click',function () {
        $(this).toggleClass('ellipsis-3lines')
    });
    $('.box-locate').on('click','.hotel-by-locate',function () {
        $('.box-locate .hotel-by-locate').removeClass('active');
        $(this).addClass('active');
        var $id = $(this).attr('city-id');
        $('.locate-picture a').addClass('hidden');
        $('.locate-picture a[city-id="'+$id+'"]').removeClass('hidden');
    });
});
