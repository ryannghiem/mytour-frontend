<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use PDO;

class HomeController extends Controller
{
    //
    public function index(){
        $data['data'] = [];
        $data['data']['slides'] = DB::connection()->table('slides')
            ->join('hotels as h','h.id','=','slides.hotel_id')
            ->join('rooms as r','r.id','=','slides.room_id')
            ->select('slides.id', 'slides.name', 'slides.description', 'slides.ordering', 'slides.image',
                'h.name as hotel_name', 'h.stars as hotel_stars',
                'r.name as room_name', 'r.size as room_size', 'r.price as room_price',
                'r.has_discount as room_has_discount', 'r.discount_percent as room_discount_percent', 'r.discount_price as room_discount_price')
            ->where('slides.status','=',1)->where('h.status','=',1)->where('r.status','=',1)
            ->orderBy('ordering')->limit(3)->get()->all();
        $cities = DB::connection()->table('cities')
            ->select('id','name','is_hot', 'hotel_count')
            ->where('cities.status','=',1)
            ->orderBy('name');
        $data['data']['cities'] = $cities->get()->all();
        $data['data']['cities_hot'] = $cities->where('is_hot','=',1)->get()->all();
        $data['data']['new_discount_hotels'] = DB::select("CALL GetHotelsByCitiesHot(6,6)");
        $data['data']['new_cities_top'] = [];
        foreach ($data['data']['new_discount_hotels'] as $hotels){
            $data['data']['new_cities_top'][$hotels->city_id] = [
                'id' => $hotels->city_id,
                'name' => $hotels->city_name
            ];
        }
        $data['data']['hot_hotels'] = DB::select("CALL GetHotHotelsByCitiesHot(6,6)");
        $data['data']['hot_cities_top'] = [];
        foreach ($data['data']['hot_hotels'] as $hotels){
            $data['data']['hot_cities_top'][$hotels->city_id] = [
                'id' => $hotels->city_id,
                'name' => $hotels->city_name
            ];
        }
        $data['data']['ads'] = DB::connection()->table('ads')
            ->select('id','name','image_origin', 'image_small', 'description','ads_url')
            ->where('status','=',1)->where('show_in_home','=',1)
            ->orderBy('ordering')->limit(3)->get()->all();;
        return view('home')->with($data);
    }
}
